var gulp = require('gulp');
var browserSync = require('browser-sync');
var config = require('./gulp.config')();
var $ = require('gulp-load-plugins')(config.pluginOpts);

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);
gulp.task('startDev', ['styles', 'inject', 'js', 'json', 'images', 'fonts', 'watch', 'browserSync']);

gulp.task('styles', function () {
  return gulp
    .src(config.sass)
    .pipe(customPlumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass())
    .pipe($.autoprefixer({browsers: 'last 25 versions'}))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(config.build + '/styles'))
    ;
});

gulp.task('html', function () {
  return gulp
    .src(config.html)
    .pipe(gulp.dest(config.build))
    ;
});

gulp.task('js', function () {
  return gulp
    .src(config.js)
    .pipe(gulp.dest(config.build + '/js'))
    ;
});

gulp.task('concatJS', function () {

  return gulp
    .src(config.js)
    .pipe(customPlumber())
    .pipe($.sourcemaps.init())
    .pipe($.concat('all.js'))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(config.build + '/js'))
    ;

});

gulp.task('json', function () {
  return gulp
    .src(config.json)
    .pipe(gulp.dest(config.build + '/json'))
    ;
});

gulp.task('images', function () {
  return gulp
    .src(config.images)
    .pipe(gulp.dest(config.build + '/images'))
    ;
});

gulp.task('css', function () {
  return gulp
    .src(config.css)
    .pipe(gulp.dest(config.build + '/styles'))
    ;
});

gulp.task('fonts', function () {
  return gulp
    .src(config.fonts)
    .pipe(gulp.dest(config.build + '/fonts'))
    ;
});

gulp.task('bower', ['vendorJS', 'vendorCSS']);

gulp.task('vendorJS', function () {
  return gulp
    .src($.mainBowerFiles())
    .pipe($.filter('**/*.js'))
    .pipe($.sourcemaps.init())
    .pipe($.concat('vendor.min.js'))
    //.pipe($.uglify())
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(config.build + '/js/vendors'))
    ;
});

gulp.task('vendorJS2', function () {
  return gulp
    .src($.mainBowerFiles())
    .pipe($.filter('**/pikaday/plugins/*.js'))
    .pipe(gulp.dest(config.build + '/js/vendors'))
    ;
});

gulp.task('vendorCSS', function () {
  return gulp
    .src($.mainBowerFiles())
    .pipe($.filter('**/*.css'))
    .pipe($.sourcemaps.init())
    .pipe($.concat('vendor.min.css'))
    .pipe($.cssmin())
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest(config.build + '/styles/vendors'))
    ;
});


gulp.task('vendorFonts', function () {

  return gulp
    .src(config.fonts)
    .pipe(gulp.dest(config.build + '/fonts'))
    ;

});

gulp.task('inject', function () {

  // var wiredepOptions = config.getWiredepOptions();
  // var wiredep = require('wiredep').stream;

  return gulp
    .src(config.html)
    .pipe($.fileInclude({
      prefix: '@@',
      file: '@file'
    }))
    .pipe(gulp.dest(config.build))
    ;

});

gulp.task('watch', ['browserSync'], function () {
  gulp.watch(config.sass, ['styles', 'bsReload']);
  gulp.watch([config.html, config.partials], ['inject', 'bsReload']);
  gulp.watch(config.js, ['js', 'bsReload']);
  gulp.watch(config.json, ['json', 'bsReload']);
});

gulp.task('browserSync', function () {
  var options = {
    proxy: 'fastpass.dev',
    files: [
      config.build + '/styles/**/*.css',
      config.build + '/js/**/*.js',
      config.build + '/*.php'
    ],
    ghostMode: {
      clicks: true,
      location: true,
      forms: true,
      scroll: true
    },
    injectChanges: true,
    notify: true,
    reloadDelay: 0
  };

  browserSync.init(null, options);
});

gulp.task('bsReload', function () {
  browserSync.reload();
});

////////// FUNCTIONS
function customPlumber() {
  return $.plumber({
    errorHandler: function (err) {
      console.log(err.stack);
      this.emit('end');
    }
  })
}







