module.exports = function(){
  var source = './src';
  var build = './build';
  var styleGuide = build + '/_styleguide';

  var config = {
    source : source,
    build: build,
    styleGuide : styleGuide,
    guide: styleGuide + '/',
    
    images: source + '/images/**/*.*',
    sass: source + '/sass/**/*.*',
    css: source + '/css/**/*.*',
    json: source + '/json/**/*.*',
    fonts: source + '/fonts/**/*.*',
    html: source + '/**/*.html',
    partials: source + '/partials/**/*.htm',

    js: [
      source + '/js/events.js',
      source + '/js/**/*.js'
    ],

    pluginOpts: {
      pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
      replaceString: /\bgulp[\-.]/
    },

    bower: {
      json: require('./bower.json'),
      directory: './bower_components/',
      ignorePath: '..'
    }
  };


  config.getWiredepOptions = function () {

    return {
      bowerJson: config.bower.json,
      directory: config.bower.directory,
      ignorePath: config.bower.ignorePath
    }
  };

  return config;
};