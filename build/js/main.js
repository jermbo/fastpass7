$(function () {
  var $body = $('body');

  // Card Items
  $('.card__titles--expandable').on('click', UserInfo.toggleSize);

  // Activity Log Functions 
  $body.on('click', '.activity__person', ActivityLog.display);
  $body.on('click', '.person__close', ActivityLog.destroy);
  $('.arrow--left').on('click', function () {
    ActivityLog.navigate('right')
  });
  $('.arrow--right').on('click', function () {
    ActivityLog.navigate('left')
  });


  // Date Picker
  $('.date-picker').on('click focus', function (e) {
    e.preventDefault();
    var $this = $(this);
    $this.pikaday({
      onSelect: function (date) {
        $this.val(moment(date).format('L'));
      }
    });
  });

  // Tool Tip Triggers
  $body.on('mouseenter', '.tip', ToolTip.createTip)
    .bind('mouseout', ToolTip.removeTip)
    .bind('mousemove', ToolTip.updateTip);

  $body.on('click', '#mainSearch', UserInfo.searchUsers);
  $body.on('click', '#mainClose', UserInfo.deleteCards);

});


$(window).load(function () {
  $('.form__item select').selectric({
    maxHeight: 200,
    arrowButtonMarkup: '<b class="arrow">&#x25be;</b>',
    responsive: true
  });
});


////////////////////////////////////////
// Update time on the site.
// Check every 10 seconds for update
// Didn't want to go to crazy and cause memory issues.
////////////////////////////////////////
(function () {
  $dateArea = $('.school__date span');
  $dateArea.text(moment().format('ddd, MMM Do YYYY, h:mm a'));
  setInterval(function () {
    $dateArea.text(moment().format('ddd, MMM Do YYYY, h:mm a'));
  }, 10000);
})();