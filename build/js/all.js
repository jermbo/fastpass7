var events = {
  events : {},
  on     : function(eventName, fn) {
    this.events[eventName] = this.events[eventName] || [];
    this.events[eventName].push(fn);
  },
  off    : function(eventName, fn) {
    if(this.events[eventName]) {
      for(var i = 0; i < this.events[eventName].length; i++) {
        if(this.events[eventName][i] === fn) {
          this.events[eventName].splice(i, 1);
          break;
        }
      }
    }
  },
  emit   : function(eventName, data) {
    if(this.events[eventName]) {
      this.events[eventName].forEach(function(fn) {
        fn(data);
      });
    }
  }
};
var $body;

$(function () {
  $body = $('body');
  resizeView($('.activity-container'));

  $body.on('click', '.activity__person', displayInfo);

  $.ajax({
      method: 'GET',
      url: 'json/people.json',
      beforeSend: function () {
        console.log('getting current users');
      }
    })
    .done(function (data) {
      displayActivityLog(data);
    })
    .fail(function (error) {
      console.error('Failed to load people : ' + error.statusText)
    });

  var originals;

  $('.card__titles--expandable').on('click', function () {
    $parent = $(this).parents('.card');

    if( !originals ){
      originals = {
        top: $parent.offset().top,
        left: $parent.offset().left,
        height: $parent.height(),
        width: $parent.width()
      };

      TweenLite.set($parent, {position: 'fixed', top: originals.top, left: originals.left, width: originals.width, height: originals.height,  zIndex: 1000});

      TweenLite.to($parent, 1, {
        height: '86vh',
        top: '12vh',
        width: '93vw',
        left: 90,
        ease: Expo.easeOut
      });

    }else{

      TweenLite.to($parent, 1, {
        height: originals.height,
        width: originals.width,
        top: originals.top,
        left: originals.left,
        ease: Expo.easeOut,
        zIndex: 10,
        onComplete: backToRelative,
        onCompleteParams: [$parent]
      });
      originals = null;

    }
    
  });

  function backToRelative(elem){
    $(elem).css({position: 'relative', zIndex: 10, width: '100%', top: 0, left: 0});
  }

  $body.on('click', '.person__close', function () {
    distroyInfo();
  });


  //////// Activity Log

  $('.arrow--left').on('click', function(){
    navigateActivityLog('right');
  });

  $('.arrow--right').on('click', function(){
    navigateActivityLog('left');
  });





  ////// Date Picker
  $('.date-picker').on('click focus', function (e) {
    e.preventDefault();
    var $this = $(this);
    $this.pikaday({
      onSelect: function (date) {
        $this.val(moment(date).format('L'));
      }
    });
  });
});

function navigateActivityLog(direction){
  distroyInfo();
  var move = (direction == 'left' ) ? '-=110' : '+=110';
  var $activityInner = $('.activity-inner');
  var $activityInnerLeft = parseInt($activityInner.css('left'));
  console.log($activityInnerLeft);
  TweenLite.to($activityInner, 0.3, {left: move, ease: Back.easeOut});
}


////////////////////////////////////////
// Update time on the site.
// Check every 10 seconds for update
// Didn't want to go to crazy and cause memory issues.
////////////////////////////////////////
(function () {
  $dateArea = $('.school__date span');
  $dateArea.text(moment().format('ddd, MMM Do YYYY, h:mm a'));
  setInterval(function () {
    $dateArea.text(moment().format('ddd, MMM Do YYYY, h:mm a'));
  }, 10000);
})();


function displayActivityLog(data) {
  for (i in data) {
    var source = $('#person-quick').html();
    var template = Handlebars.compile(source);
    var stuff = {
      status: data[i].status,
      name: data[i].name,
      image: data[i].image,
      info: JSON.stringify(data[i])
    };
    var html = template(stuff);
    $('.activity-inner').prepend(html);
  }
  updateInnerWidth();
  setUpFilter();
}

function addNewLog(data){
  var source = $('#person-quick').html();
  var template = Handlebars.compile(source);
  var stuff = {
    status: data.status,
    name: data.name,
    image: data.image,
    info: JSON.stringify(data)
  };
  var html = template(stuff);
  $('.activity-inner').prepend(html);
  TweenLite.to('.activity-inner', 0.2, { left: 0, ease: Back.easeOut });

  updateInnerWidth();
}

function updateInnerWidth(){
  var $activityInner = $('.activity-inner');
  var newWidth = $activityInner.children('.activity__person').length * 110 + 2;
  console.log('New Width : ' + newWidth);
  $activityInner.css({
    width: newWidth
  });
}

function displayInfo(e) {
  distroyInfo();
  var $this = $(this);
  var source = $('#person-details').html();
  var template = Handlebars.compile(source);
  var context = ($this.data('info'));
  var html = template(context);
  var position = {
    top: $this.offset().top - 150,
    left: $this.offset().left
  };
  $body.append(html);
  $body.find('.person').css({top: position.top, left: position.left});
}

function distroyInfo() {
  $body.find('.person').remove();
}

$(window).on('resize', function () {
  resizeView($('.activity-container'));
});

function resizeView(container) {
  var viewWidth = $(container).width(),
    maxWidth = 110,
    mult = parseInt(viewWidth / maxWidth),
    newWidth = mult * maxWidth
    ;
  $(container).children('div').first().css({'width': newWidth});
}


function setUpFilter() {
  console.log('in setup filter');


  // set up 2 way data binding here

}





//# sourceMappingURL=all.js.map
