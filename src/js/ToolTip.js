var ToolTip = (function(){
  
  function createTip(){
    $('<p class="tooltip"></p>').text($(this).attr('data-tip')).appendTo('body').fadeIn('slow');
  }
  
  function removeTip(){
    $('.tooltip').remove();
  }
  
  function updateTip(e){
    $('.tooltip').css({top: e.pageY + 5, left: e.pageX + 5});
  }
    
  return{
    createTip : createTip,
    removeTip: removeTip,
    updateTip: updateTip
  }
})();