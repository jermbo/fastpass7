var UserInfo = (function () {

  var cardOriginals;
  var isOpen;

  function toggleSize(elem) {
    ActivityLog.destroy();
    var $this = (elem.type) ? $(this) : elem;
    var $parent = $this.parents('.card');

    $('#userInputForm').show();
    if (!cardOriginals) {
      cardOriginals = {
        top: $parent.offset().top,
        left: $parent.offset().left,
        height: $parent.height() - 1,
        width: $parent.width()
      };

      TweenLite.set($parent, {
        position: 'fixed',
        top: cardOriginals.top,
        left: cardOriginals.left,
        width: cardOriginals.width,
        height: cardOriginals.height,
        zIndex: 1000
      });

      TweenLite.to($parent, 1, {
        height: '86vh',
        top: '12vh',
        width: '92%',
        left: 90,
        ease: Expo.easeOut
      });
      isOpen = true;
    } else {
      TweenLite.to($parent, 1, {
        height: cardOriginals.height,
        width: cardOriginals.width,
        top: cardOriginals.top,
        left: cardOriginals.left,
        ease: Expo.easeOut,
        zIndex: 10,
        onComplete: _backToRelative,
        onCompleteParams: [$parent]
      });
      cardOriginals = null;
    }
  }

  function _backToRelative($elem) {
    $elem.css({position: 'relative', zIndex: 10, width: '100%', top: 0, left: 0});
    isOpen = false;
  }


  function searchUsers() {
    if( $('#firstName').val() === '' || $('#lastName') === '' ){
      swal({
        title: 'Error',
        text: 'You need to fill out First Name and Last Name',
        type: 'error'
      });
      return false;
    }

    var $this = $(this);
    var $parent = $this.parents('.card__actions');

    $.ajax({
      method: 'GET',
      url: 'json/people.json',
      beforeSend: function () {
        console.log('searching database');
        $('#usersFromDB').append('<i class="loading-icon-db fa fa-spinner fa-spin fa-3x fa-fw"></i>');
        $('#userInputForm').hide();
      }
    })
      .done(function (data) {
        $('.loading-icon-db').remove();
        
        if( data.length ){
          if( !isOpen ) toggleSize( $this );
          _displayUserCards(data);
          $('#userInputForm').hide();
          $parent.append('<i id="mainClose" class="fa fa-times main-search"></i>');
          $this.remove();
        }

      })
      .fail(function (error) {
        $('#userInputForm').show();
        $('.loading-icon-db').hide();
        swal({
          title: 'Error',
          text: 'Something went wrong : ' + error.statusText,
          type: 'error'
        });
      });
  }

  function deleteUserCards(elem) {


    var $this = $(this);
    var $parent = $this.parents('.card__actions');
    toggleSize($this);
    $parent.append('<i id="mainSearch" class="fa fa-search main-search"></i>');
    $this.remove();

    $('.person--search').remove();
  }

  function _displayUserCards(data) {
    for (var i = 0; i < data.length; i++) {
      var $source = $('#person-db-details').html();
      var template = Handlebars.compile($source);
      var personData = {
        id: (i + 1),
        status: data[i].status,
        name: data[i].name,
        image: data[i].image,
        info: JSON.stringify(data[i])
      };
      var html = template(personData);

      $('#usersFromDB').prepend(html);
    }
    return 'All users added';
  }

  return {
    toggleSize: toggleSize,
    searchUsers: searchUsers,
    deleteCards: deleteUserCards
  }
})();