var ActivityLog = (function () {

  var debugMode = false;
  var $body = $('body');
  var $activityContainer = $('.activity-container');
  var $activityInner = $('.activity-inner');

  var activeUsers = [];

  var position = {
    maxLeft: 0,
    maxRight: 0,
    currentInnerPos: 0
  };

  var typingTimer;
  var typingInt = 120000;
  var $input = $("#searchLog");

  _init();

  function navigate(direction) {
    destroy();
    var move;
    position.currentInnerPos = parseInt($activityInner.css('left'));
    _calcLimits();

    if (direction == 'left' && Math.abs(position.currentInnerPos) < position.maxRight) {
      move = '-=110';
    } else if (direction == 'right' && position.currentInnerPos < position.maxLeft) {
      move = '+=110';
    }

    if (move && !TweenMax.isTweening($activityInner)) TweenMax.to($activityInner, 0.15, {
      left: move,
      ease: Power4.easeOut
    });
  }

  function _calcLimits() {
    position.maxRight = Math.abs(parseInt($activityContainer.css('width')) - parseInt($activityInner.css('width')));
  }

  function addNewItem(data) {
    for (var i = 0; i < data.length; i++) {
      var $source = $('#person-quick').html();
      var template = Handlebars.compile($source);
      var personData = {
        id: (i + 1),
        status: data[i].status,
        name: data[i].name,
        image: data[i].image,
        info: JSON.stringify(data[i])
      };
      var html = template(personData);
      activeUsers.push({
        name: personData.name.toLowerCase(),
        id: (i + 1)
      });
      $activityInner.prepend(html);
    }
    updateWidth();
    _calcLimits($activityInner.find('.activity__person').length);
    return 'All users added';
  }

  function searchActiveUsers(name) {
    var matches = [];

    for (var i in activeUsers) {
      if (activeUsers[i].name.indexOf(name) >= 0) matches.push(activeUsers[i]);
    }
    return matches;
  }

  $input.on('keyup', function (e) {
    var $this = $(this);
    var results = searchActiveUsers($this.val());
    $(".activity__person").hide();
    
    for (var i in results) {
      $("[data-id='user-" + results[i].id + "'").show();
    }

    TweenLite.set($activityInner, {left: 0});

    updateWidth();
    
    clearTimeout(typingTimer);
    typingTimer = setTimeout(_doneTyping, typingInt);
  });

  $input.on('keydown', function () {
    clearTimeout(typingTimer);
  });

  function _doneTyping() {
    $(".activity__person").show();
    $input.val('').blur();
  }

  function display(e) {
    destroy();
    var $this = $(this);
    var source = $('#person-details').html();
    var template = Handlebars.compile(source);
    var context = ($this.data('info'));
    var html = template(context);
    var position = {
      top: $this.offset().top - 150,
      left: $this.offset().left
    };

    $body.append(html);
    $body.find('.person').css({top: position.top, left: position.left});
  }

  function destroy() {
    $body.find('.person').remove();
  }

  function updateWidth() {
    // var newWidth = ($activityInner.children('.activity__person').length * 110);
    var newWidth = ($activityInner.children('.activity__person').not(':hidden').length * 110);
    console.log(newWidth);
    $activityInner.css({width: newWidth});
  }

  function resizeView() {
    var viewWidth = $activityContainer.width();
    var maxWidth = 110;
    var mult = parseInt(viewWidth / maxWidth);
    var newWidth = mult * maxWidth;

    $activityContainer.children('div').first().css({'width': newWidth});
  }

  function _init() {
    $(window).on('resize', function () {
      resizeView($activityContainer)
    });
    resizeView();
    _getCurrentUsers();
  }

  function _getCurrentUsers() {
    $.ajax({
      method: 'GET',
      url: 'json/people.json',
      beforeSend: function () {
        $('.activity-inner').css({width: '100%'}).append('<i class="loading-icon fa fa-spinner fa-spin fa-3x fa-fw"></i>');
        console.log('getting users');
      }
    })
      .done(function (data) {
        $('.loading-icon').remove();
        addNewItem(data);
      })
      .fail(function (error) {
        console.error('Failed to load people : ' + error.statusText);
      });
  }

  function refreshFeed(){
    $('.activity__person').remove();
    _getCurrentUsers();
  }


  function _cleanCallee(string) {
    var str = string.substring('function '.length);
    return str.substr(0, str.indexOf('('));
  }

  return {
    addNew: addNewItem,
    updateWidth: updateWidth,
    resizeView: resizeView,
    display: display,
    destroy: destroy,
    navigate: navigate,
    refreshFeed: refreshFeed
  }
})();